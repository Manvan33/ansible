function exec_if_exists() {
    if [ -r "$1" ]; then
        . "$1"
    fi
}

exec_if_exists ~/.profile

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
exec_if_exists "${HOME}/.p10k.zsh"

########################################################################################
# oh-my-zsh init
export ZSH="/home/n42/.oh-my-zsh"

# Powerlevel10k init
exec_if_exists "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"

# oh-my-zsh use p10k
ZSH_THEME="powerlevel10k/powerlevel10k"

# automatically update without prompting
DISABLE_UPDATE_PROMPT="true"

# enable command auto-correction
ENABLE_CORRECTION="false"

# display red dots whilst waiting for completion
COMPLETION_WAITING_DOTS="true"

# Plugins
plugins=(
  npm
  vscode
  node
  python
  docker
  docker-compose
  command-not-found
  git
  zsh-autosuggestions
  zsh-syntax-highlighting
  copybuffer
  encode64
  kubectl
  helm
  debian
)

source $ZSH/oh-my-zsh.sh
########################################################################################

export GPG_TTY=$(tty)

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

exec_if_exists "$HOME/.zsh_aliases"

export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
export EDITOR='vim'
export MANPATH="/usr/local/man:$MANPATH"

export KUBECONFIG="${KUBECONFIG}$(find $HOME/.kube/configs -type f -exec echo -n :{} \;)"
source <(kubectl completion zsh)

# remove breaking spaces
setxkbmap -option "nbsp:none"

# cargo env
. "$HOME/.cargo/env"

# startx if on tty1
if [ "$(tty)" = "/dev/tty1" ]; then
  startx
fi
