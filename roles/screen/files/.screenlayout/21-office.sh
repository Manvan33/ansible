#!/bin/bash

bash 0-pc.sh

axrandr \
    eDP-1 1920x1200 1020x2160 normal \
    DP-4  3840x2160       0x0 normal

ws2output \
    1 DP-4 \
    2 DP-4 \
    3 eDP-1
