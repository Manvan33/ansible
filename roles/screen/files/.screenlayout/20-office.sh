#!/bin/bash

bash 0-pc.sh

axrandr \
    eDP-1 1920x1200 708x1440 normal \
    DP-4  3440x1440      0x0 normal

ws2output \
    1 DP-4 \
    2 DP-4 \
    3 eDP-1
